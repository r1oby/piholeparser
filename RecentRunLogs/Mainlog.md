## Creating Main Recent Run Log. Thu Aug 15 00:20:04 UTC 2019
* Recent Run Log Recreated.
## Checking For Dependencies Thu Aug 15 00:20:16 UTC 2019
Process Took 0 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/05-Checking-For-Dependencies.md)

## Running Initial Tasks Thu Aug 15 00:20:16 UTC 2019
Process Took 4 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/10-Running-Initial-Tasks.md)

## Processing Top Level Domains Thu Aug 15 00:20:20 UTC 2019
Process Took 4 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/15-Processing-Top-Level-Domains.md)

## Processing Internal Whitelists Thu Aug 15 00:20:24 UTC 2019
Process Took 1 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/25-Processing-Internal-Whitelists.md)

## Processing External Whitelists Thu Aug 15 00:20:25 UTC 2019
Process Took 1 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/26-Processing-External-Whitelists.md)

## Processing Internal Blacklists Thu Aug 15 00:20:26 UTC 2019
Process Took 0 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/29-Processing-Internal-Blacklists.md)

## Processing External Blacklists Thu Aug 15 00:20:26 UTC 2019
Process Took 12 Minutes.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/30-Processing-External-Blacklists.md)

## Compiling Combined Blacklist Thu Aug 15 00:33:09 UTC 2019
Process Took 13 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/40-Compiling-Combined-Blacklist.md)

## Compiling Combined Whitelist Thu Aug 15 00:33:22 UTC 2019
Process Took 0 Seconds.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/45-Compiling-Combined-Whitelist.md)

## Writing Additional Lists Thu Aug 15 00:33:22 UTC 2019
Process Took 1 Minutes.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/60-Writing-Additional-Lists.md)

## Completing End Tasks Thu Aug 15 00:34:48 UTC 2019
Process Took 3 Minutes.
[Details If Any](https://github.com/deathbybandaid/piholeparser/blob/master/RecentRunLogs/TopLevelScripts/90-Completing-End-Tasks.md)

